public class HourlyEmployee extends Employee {
    
    private double hourlyPayRate;
    private int hoursWorked;
    
    public HourlyEmployee(String firstName, String lastName, String socialSecurityNumber, double hourlyPayRate, int hoursWorked) {
        super(firstName, lastName, socialSecurityNumber);
        this.hourlyPayRate = hourlyPayRate;
        this.hoursWorked = hoursWorked;
    }
    
    public double getHourlyPayRate() {
        return hourlyPayRate;
    }
    
    public void setHourlyPayRate(double hourlyPayRate) {
        this.hourlyPayRate = hourlyPayRate;
    }
    
    public int getHoursWorked() {
        return hoursWorked;
    }
    
    public void setHoursWorked(int hoursWorked) {
        this.hoursWorked = hoursWorked;
    }
    
    @Override
    public void generatePayStub() {
        double hourlyPay = hourlyPayRate * hoursWorked;
        System.out.println("Pay Stub for " + getFullName() + ":");
        System.out.println("Hourly Pay Rate: $" + hourlyPayRate);
        System.out.println("Hours Worked: " + hoursWorked);
        System.out.println("Hourly Pay: $" + hourlyPay);
    }
}
