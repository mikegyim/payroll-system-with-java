public class Main {
    
    public static void main(String[] args) {
        
        // create a SalaryEmployee object
        SalaryEmployee salaryEmployee = new SalaryEmployee("Michael", "Doe", "123-45-6789", 60000.00);
        
        // create an HourlyEmployee object
        HourlyEmployee hourlyEmployee = new HourlyEmployee("Kevin", "Smith", "987-65-4321", 25.00, 40);
        
        // generate pay stubs for the employees
        salaryEmployee.generatePayStub();
        hourlyEmployee.generatePayStub();
    }
}
