public class SalaryEmployee extends Employee {
    
    private double annualSalary;
    
    public SalaryEmployee(String firstName, String lastName, String socialSecurityNumber, double annualSalary) {
        super(firstName, lastName, socialSecurityNumber);
        this.annualSalary = annualSalary;
    }
    
    public double getAnnualSalary() {
        return annualSalary;
    }
    
    public void setAnnualSalary(double annualSalary) {
        this.annualSalary = annualSalary;
    }
    
    @Override
    public void generatePayStub() {
        double monthlyPay = annualSalary / 12.0;
        System.out.println("Pay Stub for " + getFullName() + ":");
        System.out.println("Monthly Pay: $" + monthlyPay);
    }
}
